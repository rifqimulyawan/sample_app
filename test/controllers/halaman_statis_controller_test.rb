require 'test_helper'

class HalamanStatisControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
  end

  test "should get root" do
    get halaman_statis_home_url
    assert_response :success
  end
    
  test "should get home" do
    get halaman_statis_home_url
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end

  test "should get help" do
    get halaman_statis_help_url
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end

  test "should get about" do
  	get halaman_statis_about_url
  	assert_response :success
    assert_select "title", "About | #{@base_title}"
  end

  test "should get contact" do
    get halaman_statis_contact_url
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end
end
