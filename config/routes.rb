Rails.application.routes.draw do
  root 'halaman_statis#home'
  get 'halaman_statis/home'
  get 'halaman_statis/help'
  get 'halaman_statis/about'
  get 'halaman_statis/contact'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#hello'
end
